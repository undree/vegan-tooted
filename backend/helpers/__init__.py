from configparser import ConfigParser
from datetime import datetime

from flask.json import JSONEncoder
from peewee import SqliteDatabase, PostgresqlDatabase, Database

from .instagramscrapers import *
from .models import *


class MyJSONEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.strftime("%Y-%m-%d %H:%M:%S")


def get_config_parser(filename: str) -> ConfigParser:
    config_parser = ConfigParser()

    try:
        config_parser.read_file(open(filename))
    except FileNotFoundError:
        raise FileNotFoundError("Fill in and rename `rename_to_config.ini` to `config.ini`.")

    return config_parser


def get_database(config_parser: ConfigParser) -> Database:
    database_type = config_parser["general"]["database"].lower()

    if database_type == "sqlite":
        database = SqliteDatabase(config_parser["sqlite"]["filename"])
    elif database_type in ["postgres", "postgresql"]:
        database = PostgresqlDatabase(**config_parser["postgresql"])
    else:
        raise ValueError("Invalid database type: " + database_type)

    return database
