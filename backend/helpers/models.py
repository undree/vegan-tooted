from peewee import Model, CharField, TextField, IntegerField, BooleanField, DateTimeField


class Product(Model):
    # Database is bound externally with database.bind([Product])
    shortcode = CharField(primary_key=True, max_length=16)
    name = TextField()
    brand = TextField()
    price = IntegerField()  # euro cents (actual price * 100)
    description = TextField()
    location = TextField()
    vegan = BooleanField()
    gluten_free = BooleanField()
    sugar_free = BooleanField()
    palm_oil_free = BooleanField()
    likes = IntegerField()
    comments = IntegerField()
    timestamp = DateTimeField()
    # image_url = TextField()
    full_caption = TextField()
    # TODO: Hashtags as many-to-many relationship


class Hashtag(Model):
    pass
