import Vue from "vue";
import App from "./App.vue";
import { auto as followSystemColorScheme } from "darkreader";

followSystemColorScheme();

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
}).$mount("#app");
